# *Euclid for SketchUp*

*Euclid for SketchUp* is a free and open-source extension that makes it easy to create and modify the geometry inputs for building energy models. *Euclid* builds on the *Legacy OpenStudio* extension for [*EnergyPlus*](https://energyplus.net). *Euclid* will continue to support reading and writing of *EnergyPlus* geometry, but will also add new features and capabilities to read and write other energy model formats.

## Download

You can download the [latest version of *Euclid* here](http://downloads.bigladdersoftware.com/?ref=euclid-latest-rbz) or select from the release versions below. See the file **releases.md** for notes on each version.

### Releases

- [*Euclid 0.9.3*](http://downloads.bigladdersoftware.com/?ref=euclid-0.9.3.rbz) (.rbz file; compatible with *EnergyPlus 8.7* and *SketchUp 2016* or higher, *SketchUp 2015* may also work)
- [*Euclid 0.9.2*](http://downloads.bigladdersoftware.com/?ref=euclid-0.9.2.rbz) (.rbz file; compatible with *EnergyPlus 8.6* and *SketchUp 2016* or higher, *SketchUp 2015* may also work)
- [*Euclid 0.9.1*](http://downloads.bigladdersoftware.com/?ref=euclid-0.9.1.rbz) (.rbz file; compatible with *EnergyPlus 8.6* and *SketchUp 2016* or higher, *SketchUp 2015* may also work)
- [*Euclid 0.9.0*](http://downloads.bigladdersoftware.com/?ref=euclid-0.9.0.rbz) (.rbz file; compatible with *EnergyPlus 8.4* and *SketchUp 2016* or higher, *SketchUp 2015* may also work)

## Install/Uninstall

*Euclid* is installed using the Extension Manager in *SketchUp*. You can find [instructions here](https://help.sketchup.com/en/article/3000263#install-manual) on how to install extensions manually via the Extension Manager. (NOTE: *Euclid* is not currently available on the Extension Warehouse.)

You can also use the Extension Manager to [uninstall](https://help.sketchup.com/en/article/3000264#uninstall-extension) *Euclid*. If you just want to temporarily turn off *Euclid*, you can [disable and enable extensions](https://help.sketchup.com/en/article/3000264#enable-extension) using the Extension Manager.

## Usage

For the time being, *Euclid* is still sufficiently similar to the *Legacy OpenStudio* extension that you can refer to the **Legacy OpenStudio User Guide** for documentation and tutorials. You can open the guide from inside *SketchUp* by selecting **Extensions > Euclid > Legacy OpenStudio User Guide** or by clicking the corresponding toolbar button.

## Support

To get help with *Euclid* you can send us an email at <euclid@bigladdersoftware.com> or consider posting your question on [*Unmet Hours*](https://unmethours.com/questions/)--a question-and-answer website for building energy modelers.

## License

See the file **license.txt**.

## Acknowledgments

Funding to develop *Euclid* has been provided by the California Energy Commission.

*Euclid* is based on the *Legacy OpenStudio* extension originally developed by the National Renewable Energy Laboratory for the United States Department of Energy.
