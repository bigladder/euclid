# Copyright (c) 2017-2020 Big Ladder Software LLC. All rights reserved.
# See the file "license.txt" for additional terms and conditions.

module Euclid
  VERSION = "0.9.4.3"
end
